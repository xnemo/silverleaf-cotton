from django.db.models import Count, Sum
from django.db.models.functions import Coalesce

from main.queryset.page import PageQuerySet


class MoundQuerySet(PageQuerySet):

    def get_with_modules(self):
        return super()\
            .annotate(modules_count=Count('modules'))\
            .annotate(modules_weight=Coalesce(Sum('modules__weight'), 0.0))
