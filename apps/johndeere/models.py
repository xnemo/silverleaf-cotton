from django.db import models
from core.models import BaseModel
from django.utils.translation import ugettext_lazy as _


class JohnDeerToken(BaseModel):
    token_type = models.CharField(_('Token type'), max_length=255, blank=False)
    expires_in = models.IntegerField(_('Expires in'), blank=True, null=True)
    access_token = models.TextField(_('Access token'), blank=False)
    scope = models.CharField(_('Scope'), max_length=255, blank=False)
    refresh_token = models.CharField(_('Refresh token'), max_length=255, blank=False)
    expires_at = models.FloatField(_('Expires at'))

    class Meta:
        verbose_name = _('JohnDeer token')
        verbose_name_plural = _('JohnDeer tokens')

    def __str__(self):
        return self.token_type