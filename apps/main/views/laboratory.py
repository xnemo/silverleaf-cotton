from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from main.models import ModuleTrace


@login_required
def list(request):
    modules = ModuleTrace.objects.get_reception_modules().per_page(request, 10)
    return render(request, "main/laboratory/list.html", {'data': modules})


@login_required
def moisture_update(request, serial_number):
    module = get_object_or_404(ModuleTrace, serial_number=serial_number)
    module.moisture = request.POST.get('value', 0)
    module.save()
    return HttpResponse('')
