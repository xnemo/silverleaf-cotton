import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from main.forms import MoundForm
from main.models import Mound, Field, Module
from django.shortcuts import render, get_object_or_404


@login_required
def list(request):
    mounds = Mound.objects.order_by('id').get_with_modules().per_page(request, 9)
    return render(request, "main/mound/list.html", {'data': mounds})


@login_required
def create(request):
    if request.method == 'POST':
        form = MoundForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('mounds-list')

    else:
        form = MoundForm()

    form.fields['field'].queryset = Field.objects.get_has_modules().order_by('-modules_count')
    return render(request, "main/mound/create.html", {'form': form})


@login_required
def create_all(request):
    fields = Field.objects.get_has_modules()
    for field in fields:
        values_for_update = {
            'uid': field.john_deere_id,
            'field': field,
            'name': field.name
        }
        Mound.objects.update_or_create(uid=field.john_deere_id, defaults=values_for_update)
    return redirect('mounds-list')


@login_required
def update(request, id):
    mound = get_object_or_404(Mound, id=id)
    form = MoundForm(initial={'name': mound.name, 'field': mound.field})
    if request.method == 'POST':
        form = MoundForm(request.POST, instance=mound)
        if form.is_valid():
            form.save()
            return redirect('mounds-list')

    form.fields['field'].queryset = Field.objects.get_has_modules().order_by('-modules_count')
    return render(request, "main/mound/edit.html", {'form': form})


@login_required
def remove(request, id):
    mound = get_object_or_404(Mound, id=id)
    mound.delete()
    return redirect('mounds-list')


@login_required
def modules(request, id):
    mound = get_object_or_404(Mound, id=id)
    unloaded_modules = Module.objects.get_unloaded(mound.field.id)
    loaded_modules = Module.objects.filter(mound=mound).order_by('-id').per_page(request, 30)
    all_loaded_modules = Module.objects.filter(mound=mound)
    total_moisture = sum(m.moisture for m in all_loaded_modules)
    total_weight = sum(m.weight for m in all_loaded_modules)
    avg_moisture = 0
    if all_loaded_modules.count() > 0:
        avg_moisture = round(total_moisture / all_loaded_modules.count(), 2)

    if request.method == 'POST':
        module_id = request.POST.get('module')
        module = Module.objects.get(id=module_id)
        module.load_time = datetime.datetime.now()
        module.mound = mound
        module.save()
        return redirect('mounds-modules', id=mound.id)

    return render(request, "main/mound/modules.html",
                  {'mound': mound,
                   'unloaded_modules': unloaded_modules,
                   'modules': loaded_modules,
                   'avg_moisture': avg_moisture,
                   'total_weight': total_weight
                   })


@login_required
def start_process(request, id):
    mound = get_object_or_404(Mound, id=id)
    mound.state = Mound.State.IN_PROCESS
    mound.start_production_time = datetime.datetime.now()
    mound.save()
    return redirect('bales-production', mound_id=mound.id)