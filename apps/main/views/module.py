from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404

from main.models import Module, Field


@login_required
def list(request):
    modules = Module.objects.all().order_by('-state').per_page(request, 10)
    return render(request, "main/modules.html", {'data': modules})


@login_required
def fields(request, uid):
    field = get_object_or_404(Field, john_deere_id=uid)
    modules = Module.objects.filter(field_uid=uid).order_by('-data_ingestion_date').all()
    data = Module.objects.filter(field_uid=uid).order_by('-data_ingestion_date').per_page(request, 30)
    total_weight = sum(m.weight for m in modules)
    total_moisture = sum(m.moisture for m in modules)
    avg_moisture = 0
    avg_weight = 0
    if modules.count() > 0:
        avg_weight = round(total_weight/modules.count(), 2)
        avg_moisture = round(total_moisture/modules.count(), 2)
    return render(request, "main/field-modules.html",
                  {'field': field,
                   'modules': modules,
                   'total_weight': total_weight,
                   'moisture_avg': avg_moisture,
                   'weight_avg': avg_weight,
                   'data': data
                   })
