from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import UpdateAPIView, CreateAPIView

from api.serializers import ModuleInFieldSerializer, ModulePickSerializer, ModuleTraceSerializer
from main.models import Module


class TransportationInFieldView(UpdateAPIView):
    http_method_names = ['put']
    serializer_class = ModuleInFieldSerializer

    def put(self, request, uid):
        module = get_object_or_404(Module, uid=uid)
        serializer = self.serializer_class(module, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)


class TransportationPickView(UpdateAPIView):
    http_method_names = ['put']
    serializer_class = ModulePickSerializer

    def put(self, request, uid):
        module = get_object_or_404(Module, uid=uid)
        serializer = self.serializer_class(module, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)


class TransportationTraceView(CreateAPIView):
    serializer_class = ModuleTraceSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)