import datetime

from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render

from main.forms import ProductionForm
from main.models import Bale, Module, Mound, ModuleTrace


@login_required
def list(request):
    bales = Module.objects.filter(state=Module.State.PROCESSED).order_by('-end_production').per_page(request, 20)
    return render(request, "main/bale/list.html", {'data': bales})


@login_required
def production(request, mound_id=None, module_id=None):

    mound = Mound.objects.get(id=mound_id) if mound_id is not None else None
    module_trace_query = ModuleTrace.objects.filter(mound_uid=mound.uid).filter(state__lt=ModuleTrace.State.PROCESSED)

    module_trace = module_trace_query.order_by('-time').first()
    bales = Bale.objects.filter(mound_id=mound_id).order_by('-id')

    form = ProductionForm(initial={'mound': mound, 'module_trace': module_trace})
    # form.fields['mound'].disabled = True
    # form.fields['module'].queryset = modules_query.order_by('-load_time')
    # form.fields['module'].disabled = True
    form.fields['module_trace'].queryset = module_trace_query.order_by('-time')

    if request.method == 'POST':
        form = ProductionForm(request.POST)
        if form.is_valid():
            module = form.instance.module_trace
            bales_count = module.bales.count()
            next_bale = bales_count + 1

            if bales_count == 0:
                module.start_production = datetime.datetime.now()
                module.state = Module.State.IN_PROCESS

            if next_bale >= 3:
                module.end_production = datetime.datetime.now()
                module.state = Module.State.PROCESSED

            module.save()

            form.instance.uid = f"UZB057{module.serial_number}{next_bale}{module.time.year}"
            form.save()

            if mound.modules.filter(state__lt=Module.State.PROCESSED).count() == 0:
                mound.state = mound.State.PROCESSED
                mound.end_production_time = datetime.datetime.now()
                mound.save()

            return HttpResponseRedirect(request.path_info)

    return render(request, "main/bale/production.html", {'form': form, 'bales': bales})


@login_required
def mound_module(request, id):
    modules = Module.objects.filter(mound_id=id).filter(state__lt=Module.State.PROCESSED).order_by('-load_time')
    serialized = serializers.serialize('json', modules)
    return JsonResponse(serialized, safe=False)


@login_required
def print_bale(request):
    pass
