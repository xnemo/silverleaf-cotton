import time

import requests

from johndeere.services.auth import get_valid_token
from main.models import Farm, Field


def get_farms(organization_id):
    token = get_valid_token()
    headers = {
        'Authorization': 'Bearer ' + token, 'Accept': 'application/vnd.deere.axiom.v3+json',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
    }
    has_next_page = True
    url = f"https://sandboxapi.deere.com/platform/organizations/{organization_id}/farms;start=0;count=100?recordFilter=ALL"
    while has_next_page:
        result = requests.get(url, headers=headers).json()
        has_next_page = False
        for item in result['values']:
            values_for_update = {
                'name': item['name'],
                'john_deere_id': item['id'],
                'archived': item['archived']
            }
            Farm.objects.update_or_create(john_deere_id=item['id'], defaults=values_for_update)
            # get_farm_fields(organization_id, item['id'], farm.id, headers)
        for link in result['links']:
            if link['rel'] == 'nextPage':
                url = link['uri']
                has_next_page = True

        time.sleep(2)


def get_farm_fields(organization_id, farm_uid, farm_id):
    token = get_valid_token()
    headers = {
        'Authorization': 'Bearer ' + token, 'Accept': 'application/vnd.deere.axiom.v3+json',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
    }
    has_next_page = True
    url = f"https://sandboxapi.deere.com/platform/organizations/{organization_id}/farms/{farm_uid}/fields;start=0;count=100"
    while has_next_page:
        result = requests.get(url, headers=headers).json()
        has_next_page = False
        for item in result['values']:
            values_for_update = {
                'name': item['name'],
                'john_deere_id': item['id'],
                'archived': item['archived'],
                'last_modified_time': item['lastModifiedTime'],
                'farm_id': farm_id,
                'farm_uid': farm_uid
            }
            Field.objects.update_or_create(john_deere_id=item['id'], defaults=values_for_update)

        for link in result['links']:
            if link['rel'] == 'nextPage':
                url = link['uri']
                has_next_page = True
        time.sleep(2)
