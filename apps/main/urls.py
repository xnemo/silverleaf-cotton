from django.urls import path
from main.views import mound, dashboard, organization, farm, field, machine, module, bale, laboratory

urlpatterns = [
    path('', dashboard.index, name='home'),
    path('organizations', organization.list, name='organization-list'),
    path('farms', farm.list, name='farms-list'),
    path('farms-json', farm.json_list, name='farms-list-json'),
    path('fields', field.list, name='fields-list'),
    path('machines', machine.list, name='machines-list'),
    path('modules', module.list, name='modules-list'),
    path('fields/<str:uid>/modules', module.fields, name='field-modules'),
    path('mounds', mound.list, name='mounds-list'),
    path('mounds/create', mound.create, name='mounds-create'),
    path('mounds/create-all', mound.create_all, name='mounds-create-all'),
    path('mounds/edit/<int:id>', mound.update, name='mounds-edit'),
    path('mounds/remove/<int:id>', mound.remove, name='mounds-remove'),
    path('mounds/modules/<int:id>', mound.modules, name='mounds-modules'),
    path('mound/start/production/<int:id>', mound.start_process, name='mounds-start-production'),
    path('mound/modules/json/<int:id>', bale.mound_module, name='bale-mound-modules'),
    path('bales', bale.list, name='bales-list'),
    path('bales/production/<int:mound_id>/<int:module_id>', bale.production, name='bales-production'),
    path('bales/production/<int:mound_id>', bale.production, name='bales-production'),
    path('bales/production', bale.production, name='bales-production'),
    path('laboratory/modules', laboratory.list, name='laboratory-modules'),
    path('laboratory/modules/moisture/<str:serial_number>', laboratory.moisture_update, name='laboratory-moisture')
]