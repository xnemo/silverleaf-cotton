from celery import shared_task
from celery.utils.log import get_task_logger

from johndeere.services.farms import get_farms, get_farm_fields
from johndeere.services.modules import get_modules
from johndeere.services.fields import get_fields

logger = get_task_logger(__name__)


@shared_task(autoretry_for=(Exception,), retry_kwargs={'max_retries': 5})
def get_farms_task(org_id):
    get_farms(org_id)


@shared_task(autoretry_for=(Exception,), retry_kwargs={'max_retries': 5})
def get_fields_by_farm(org_id, farm_uid, farm_id):
    get_farm_fields(org_id, farm_uid, farm_id)


@shared_task(autoretry_for=(Exception,), retry_kwargs={'max_retries': 5})
def get_modules_task(org_id):
    get_modules(org_id)


@shared_task(autoretry_for=(Exception,), retry_kwargs={'max_retries': 5})
def get_fields_task(org_id):
    get_fields(org_id, logger)

