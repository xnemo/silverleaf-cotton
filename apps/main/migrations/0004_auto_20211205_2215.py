# Generated by Django 3.2.8 on 2021-12-05 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20211129_2201'),
    ]

    operations = [
        migrations.AddField(
            model_name='farm',
            name='archived',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='field',
            name='archived',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='field',
            name='last_modified_time',
            field=models.DateTimeField(default=None, verbose_name='Last Modified Time'),
            preserve_default=False,
        ),
    ]
