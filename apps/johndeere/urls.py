from django.urls import path
from johndeere import views

urlpatterns = [
    path('john-auth', views.auth, name='john-auth'),
    path('callback', views.callback),
    path('sync-organizations', views.organization_sync, name='sync-organizations'),
    path('sync-farms', views.farms_sync, name='sync-farms'),
    path('sync-fields', views.fields_sync, name='sync-fields'),
    path('sync-machines', views.machines_sync, name='sync-machines'),
    path('sync-modules', views.modules_sync, name='sync-modules')
]
