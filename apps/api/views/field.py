from django.contrib.postgres.search import SearchVector

from rest_framework import filters
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from api.pagination import DatatablePagination
from api.queryset.datatable import filter_for_datatable
from api.serializers import FieldSerializer
from main.models import Field


class FieldDatatableView(ListAPIView):
    serializer_class = FieldSerializer
    pagination_class = DatatablePagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)

    def get_queryset(self):
        return Field.objects.get_has_modules().all()

    def list(self, request, *args, **kwargs):
        draw = request.query_params.get('draw')
        queryset = self.filter_queryset(self.get_queryset())
        records_total = queryset.count()
        search_vector = SearchVector('name', 'farm__name', 'created_at')
        filtered_queryset = filter_for_datatable(request, queryset, search_vector)
        start = int(request.query_params.get('start', 0))
        length = int(request.query_params.get('length', 10))
        end = length + start
        serializer = self.get_serializer(filtered_queryset[start:end], many=True)
        response = {
            'draw': draw,
            'recordsTotal': records_total,
            'recordsFiltered': filtered_queryset.count(),
            'data': serializer.data
        }
        return Response(response)
