from django.contrib import admin

from main.models import Farm, Field, Equipment, Module, Mound, Country, Region, District, BaleTrace, Bale, ModuleTrace


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')
    fields = ('code', 'name')
    search_fields = ('code', 'name')


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'country', 'created_at')
    fields = ('name', 'code', 'country')
    search_fields = ('name', 'code', 'country')


@admin.register(District)
class DistrictAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'region', 'created_at')
    fields = ('name', 'code', 'region')
    search_fields = ('name', 'code', 'region')


@admin.register(Farm)
class FarmAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'district', 'john_deere_id', 'created_at')
    fields = ('name', 'description', 'district')
    search_fields = ('name', 'description', 'district')


@admin.register(Field)
class FieldAdmin(admin.ModelAdmin):
    list_display = ('name', 'varieties', 'farm', 'geo_json', 'john_deere_id', 'created_at')
    fields = ('name', 'varieties', 'farm', 'geo_json')
    search_fields = ('name', 'varieties', 'farm')


@admin.register(Equipment)
class EquipmentAdmin(admin.ModelAdmin):
    list_display = ('guid', 'description', 'john_deere_id', 'created_at')
    fields = ('guid', 'description')
    search_fields = ('guid', 'description')


@admin.register(Mound)
class MoundAdmin(admin.ModelAdmin):
    list_display = ('uid', 'name', 'created_at', 'start_production_time', 'end_production_time', 'state')
    fields = ('uid', 'name')
    search_fields = ('uid', 'name')


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    list_display = ('uid', 'farm', 'field', 'machine_pin', 'diameter', 'weight', 'moisture', 'mound',
                    'data_ingestion_date', 'drop_lat', 'drop_long', 'wrap_date_time', 'wrap_lat', 'wrap_long',
                    'created_at', 'load_time', 'register_in_field_time', 'in_field_lat', 'in_field_long',
                    'picked_from_field_time', 'pick_lat', 'pick_long')
    fields = ('uid', 'farm', 'field', 'equipment', 'diameter', 'weight', 'moisture', 'mound')
    search_fields = ('uid', 'farm', 'field', 'equipment', 'diameter', 'weight', 'moisture', 'mound')


# @admin.register(MoundModules)
# class MoundModulesAdmin(admin.ModelAdmin):
#     list_display = ('scan_date', 'farm', 'field', 'module', 'mound', 'module_weight')
#     fields = ('scan_date', 'farm', 'field', 'module', 'mound', 'module_weight')
#     search_fields = ('scan_date', 'farm', 'field', 'module', 'mound', 'module_weight')


@admin.register(Bale)
class BaleLabelAdmin(admin.ModelAdmin):
    list_display = ('print_date', 'module', 'mound', 'uid', 'production_date', 'weight')
    fields = ('print_date', 'module', 'mound', 'uid', 'production_date', 'weight')
    search_fields = ('print_date', 'module', 'mound', 'uid', 'production_date', 'weight')


@admin.register(BaleTrace)
class BaleTraceAdmin(admin.ModelAdmin):
    list_display = ('module', 'bale_uid', 'time', 'lat', 'long', 'description', 'action', 'state')
    fields = ('module', 'bale_uid', 'time', 'lat', 'long', 'description', 'action', 'state')
    search_fields = ('module', 'bale_uid', 'time', 'lat', 'long', 'description', 'action', 'state')


@admin.register(ModuleTrace)
class ModuleTraceAdmin(admin.ModelAdmin):
    list_display = ('serial_number', 'action', 'time', 'lat', 'long', 'mound_uid', 'truck_id', 'position', 'moisture')
    fields = ('serial_number', 'action', 'time', 'lat', 'long')
    search_fields = ('serial_number', 'action', 'time', 'mound_uid', 'truck_id')
