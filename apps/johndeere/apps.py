from django.apps import AppConfig


class JohndeereConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'johndeere'
