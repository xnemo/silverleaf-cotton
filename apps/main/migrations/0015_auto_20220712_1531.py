# Generated by Django 3.2.8 on 2022-07-12 10:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_auto_20220212_1803'),
    ]

    operations = [
        migrations.AddField(
            model_name='module',
            name='pick_lat',
            field=models.FloatField(null=True, verbose_name='Pick lat'),
        ),
        migrations.AddField(
            model_name='module',
            name='pick_long',
            field=models.FloatField(null=True, verbose_name='Pick long'),
        ),
        migrations.AddField(
            model_name='module',
            name='picked_from_field_time',
            field=models.DateTimeField(null=True, verbose_name='Picked from field'),
        ),
        migrations.AlterField(
            model_name='bale',
            name='module',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='bales', to='main.module', verbose_name='Module'),
        ),
        migrations.AlterField(
            model_name='bale',
            name='mound',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='bales', to='main.mound', verbose_name='Mounds'),
        ),
    ]
