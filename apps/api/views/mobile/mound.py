from rest_framework import filters
from rest_framework.generics import ListAPIView, UpdateAPIView, get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework import status

from api.serializers import MoundListSerializer, ModuleReceptionSerializer
from main.models import Mound, Module


class MoundView(ListAPIView):
    serializer_class = MoundListSerializer
    pagination_class = PageNumberPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('uid', 'name')
    ordering = ['pk']

    def get_queryset(self):
        return Mound.objects.all()


class ReceptionView(UpdateAPIView):
    http_method_names = ['put']
    serializer_class = ModuleReceptionSerializer

    def put(self, request, uid):
        module = get_object_or_404(Module, uid=uid)
        serializer = self.serializer_class(module, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)
