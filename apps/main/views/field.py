from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from main.models import Field


@login_required
def list(request):
    data = Field.objects.get_has_modules().order_by('-modules_count').per_page(request, 10)
    return render(request, "main/fields.html", {'data': data})
