from django.db import models
from main.queryset.field import FieldQuerySet

FieldManager = models.Manager.from_queryset(FieldQuerySet)