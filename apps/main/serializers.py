from rest_framework import serializers
from main.models import Farm


class FarmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Farm
        fields = ('id', 'name', 'district', 'john_deere_id', 'created_at')