from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class BaseModel(models.Model):
    created_by = models.ForeignKey(User, models.CASCADE, related_name='created_%(model_name)s', editable=False,
                                   null=True, blank=True, verbose_name=_('Created by user'))
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, verbose_name=_('Created time'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, null=True, verbose_name=_('Updated time'))

    class Meta:
        abstract = True
        ordering = ('id',)
        get_latest_by = 'created_at'
