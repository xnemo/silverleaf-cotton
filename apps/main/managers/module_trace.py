from django.db import models
from main.queryset.module_trace import ModuleTraceQuerySet

ModuleTraceManager = models.Manager.from_queryset(ModuleTraceQuerySet)