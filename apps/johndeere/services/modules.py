import time
from datetime import datetime

from core.utils.requests import requests_retry_session
from johndeere.services.auth import get_valid_token
from main.models import Module, Field
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


def get_modules(organization_id):
    token = get_valid_token()
    headers = {'Authorization': 'Bearer ' + token,
               'Accept': 'application/vnd.deere.axiom.v3+json',
               'Accept-UOM-System': 'METRIC',
               'Accept-Yield-Preference': 'MASS'}

    start_date = "2022-09-01T00:00:00Z"
    last_module = Module.objects.order_by('-wrap_date_time').first()
    if last_module:
        start_date = last_module.wrap_date_time.strftime("%Y-%m-%dT%H:%M:%SZ")
    end_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")

    has_next_page = True
    url = f"https://sandboxapi.deere.com/platform/organizations/{organization_id}/harvestIdentificationModules;" \
          f"start=0;count=100?" \
          f"startDate={start_date}&endDate={end_date}"

    while has_next_page:
        result = requests_retry_session().get(url, headers=headers).json()
        has_next_page = False
        logger.info('URL: ' + url)
        for item in result['values']:
            logger.info('Start save: ' + item['moduleSerialNumber'])
            field = Field.objects.filter(john_deere_id=item['fieldId']).first()
            # if field:
            try:
                search_module = Module.objects.filter(serial_number=item['moduleSerialNumber']).exists()
                if not search_module:
                    Module.objects.create(
                        serial_number=item['moduleSerialNumber'],
                        uid=item['moduleId'],
                        wrap_lat=item['wrapLocation']['lat'] if item.get('wrapLocation') is not None else None,
                        wrap_long=item['wrapLocation']['lon'] if item.get('wrapLocation') is not None else None,
                        wrap_date_time=item['wrapDateTime'],
                        data_ingestion_date=item['dataIngestionDate'],
                        tag_count=item['tagCount'],
                        variety_name=item['varietyName'],
                        machine_pin=item['machinePin'],
                        operator=item['operator'],
                        moisture=item['moisture']['value'] if item.get('moisture') is not None else None,
                        moisture_unit=item['moisture']['unitId'] if item.get('moisture') is not None else None,
                        diameter=item['diameter']['value'] if item.get('diameter') is not None else None,
                        diameter_unit=item['diameter']['unitId'] if item.get('diameter') is not None else None,
                        weight=item['weight']['value'] if item.get('weight') is not None else None,
                        weight_unit=item['weight']['unitId'] if item.get('weight') is not None else None,
                        drop_long=item['dropLocation']['lon'] if item.get('dropLocation') is not None else None,
                        drop_lat=item['dropLocation']['lat'] if item.get('dropLocation') is not None else None,
                        incremental_area=item['incrementalArea']['value'] if item.get('incrementalArea') is not None else None,
                        area_unit=item['incrementalArea']['unitId'] if item.get('incrementalArea') is not None else None,
                        field_uid=item['fieldId'],
                        field=field,
                        farm_id=field.farm_id
                    )
            except:
                logger.info('ERROR: ' + item['moduleSerialNumber'])

        for link in result['links']:
            if link['rel'] == 'nextPage':
                url = link['uri']
                has_next_page = True

        time.sleep(4)
