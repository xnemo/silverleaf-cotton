from django.db import models
from main.queryset.module import ModuleQuerySet

ModuleManager = models.Manager.from_queryset(ModuleQuerySet)
