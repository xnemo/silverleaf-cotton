from django.urls import path, re_path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from api.views.equipment import EquipmentDatatableView
from api.views.farm import FarmDatatableView, FarmDetailView
from api.views.field import FieldDatatableView
from api.views.mobile.transportation import TransportationInFieldView, TransportationPickView, TransportationTraceView
from api.views.module import ModuleView, ModuleDatatableView
from api.views.mobile.mound import MoundView, ReceptionView
from api.views.mobile.ginning import ProcessStartView, GinningReceptionView, GinningStorageView, GinningProcessFinishView

urlpatterns = [
    re_path(r'^auth/token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    re_path(r'^auth/token/refresh/$', TokenRefreshView.as_view(), name='token_refresh'),
    # re_path(r'^auth/logout/$', LogoutView.as_view(), name='auth_logout'),

    re_path(r'^farm/$', FarmDatatableView.as_view(), name='farms_datatable'),
    path('farm/<int:pk>', FarmDetailView.as_view(), name='farm_detail_view'),
    path('module', ModuleView.as_view(), name='module_view'),
    re_path(r'^field/$', FieldDatatableView.as_view(), name='field_datatable'),
    re_path(r'^modules/$', ModuleDatatableView.as_view(), name='module_datatable'),
    re_path(r'^equipment/$', EquipmentDatatableView.as_view(), name='equipment_datatable'),

    path('transportation/field/in/<str:uid>', TransportationInFieldView.as_view(), name='transportation_field_in'),
    path('transportation/field/pick/<str:uid>', TransportationPickView.as_view(), name='transportation_field_pick'),
    re_path(r'^mound/$', MoundView.as_view(), name='mound_list'),
    path('ginning/reception', GinningReceptionView.as_view(), name='ginning_reception'),
    path('transportation/trace', TransportationTraceView.as_view(), name='module_transportation_trace'),
    path('ginning/process/start/<str:uid>', ProcessStartView.as_view(), name='ginning_process_start'),
    path('ginning/bale/storage', GinningStorageView.as_view(), name='bale_storage'),
    path('ginning/process/finish', GinningProcessFinishView.as_view(), name='ginning_process_finish'),
]


