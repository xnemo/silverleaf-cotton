# Generated by Django 3.2.8 on 2022-07-21 18:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_auto_20220712_1531'),
    ]

    operations = [
        migrations.AddField(
            model_name='module',
            name='in_field_lat',
            field=models.FloatField(null=True, verbose_name='In field lat'),
        ),
        migrations.AddField(
            model_name='module',
            name='in_field_long',
            field=models.FloatField(null=True, verbose_name='In field long'),
        ),
        migrations.AddField(
            model_name='module',
            name='register_in_field_time',
            field=models.DateTimeField(null=True, verbose_name='Register on field'),
        ),
    ]
