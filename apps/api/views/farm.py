from django.contrib.postgres.search import SearchVector
from django.shortcuts import get_object_or_404

from rest_framework import filters, status
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response

from api.pagination import DatatablePagination
from api.queryset.datatable import filter_for_datatable
from api.serializers import FarmSerializer
from main.models import Farm


class FarmDatatableView(ListAPIView):
    serializer_class = FarmSerializer
    pagination_class = DatatablePagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'description')
    ordering = ['pk']

    def get_queryset(self):
        return Farm.objects.all()

    def list(self, request, *args, **kwargs):
        draw = request.query_params.get('draw')
        queryset = self.filter_queryset(self.get_queryset())
        records_total = queryset.count()
        search_vector = SearchVector('name', 'description', 'district__name', 'created_at')
        filtered_queryset = filter_for_datatable(request, queryset, search_vector)
        start = int(request.query_params.get('start', 0))
        length = int(request.query_params.get('length', 10))
        end = length + start
        serializer = self.get_serializer(filtered_queryset[start:end], many=True)
        response = {
            'draw': draw,
            'recordsTotal': records_total,
            'recordsFiltered': filtered_queryset.count(),
            'data': serializer.data
        }
        return Response(response)


class FarmDetailView(RetrieveAPIView):
    serializer_class = FarmSerializer

    def get(self, request, pk):
        farm = get_object_or_404(Farm, id=pk)
        serializer = self.serializer_class(farm)
        return Response(serializer.data, status=status.HTTP_200_OK)