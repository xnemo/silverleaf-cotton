import json
import logging
import uuid
from datetime import datetime

import requests
from django.conf import settings
from requests_oauthlib import OAuth2Session

from johndeere.models import JohnDeerToken


def get_well_known():
    well_known_url = settings.JOHN_DEERE_API.get('WELL_KNOWN')
    well_known_response = requests.get(well_known_url)
    well_known_info = json.loads(well_known_response.text)
    return well_known_info


def authorize():
    client_id = settings.JOHN_DEERE_API.get('CLIENT_ID')
    callback_url = settings.JOHN_DEERE_API.get('CALLBACK_URL')
    scope = settings.JOHN_DEERE_API.get('SCOPES')
    state = str(uuid.uuid1())

    well_known = get_well_known()
    auth_url = well_known['authorization_endpoint']
    john_deere = OAuth2Session(client_id=client_id, redirect_uri=callback_url, scope=scope, state=state,)
    authorization_url, state = john_deere.authorization_url(auth_url)
    return authorization_url, state


def get_token(state, uri):
    try:
        client_id = settings.JOHN_DEERE_API.get('CLIENT_ID')
        client_secret = settings.JOHN_DEERE_API.get('CLIENT_SECRET')
        callback_url = settings.JOHN_DEERE_API.get('CALLBACK_URL')
        scope = settings.JOHN_DEERE_API.get('SCOPES')
        well_known = get_well_known()
        token_url = well_known['token_endpoint']

        john_deere = OAuth2Session(client_id=client_id, redirect_uri=callback_url, state=state, scope=scope,)
        token = john_deere.fetch_token(token_url=token_url, client_secret=client_secret, authorization_response=uri)
        print(type(token))

        JohnDeerToken.objects.create(
            token_type=token['token_type'],
            expires_in=token['expires_in'],
            access_token=token['access_token'],
            refresh_token=token['refresh_token'],
            expires_at=token['expires_at'],
            scope=token['scope']
        )

    except Exception as e:
        logging.exception(e)
        return 'Error getting token!'


def refresh_access_token(refresh_token):
    try:
        client_id = settings.JOHN_DEERE_API.get('CLIENT_ID')
        client_secret = settings.JOHN_DEERE_API.get('CLIENT_SECRET')
        callback_url = settings.JOHN_DEERE_API.get('CALLBACK_URL')
        scope = settings.JOHN_DEERE_API.get('SCOPES')
        well_known = get_well_known()
        token_url = well_known['token_endpoint']

        extra = {
            'client_id': client_id,
            'client_secret': client_secret,
        }

        john_deere = OAuth2Session(client_id, scope=scope)
        token = john_deere.refresh_token(token_url, refresh_token=refresh_token, **extra)

        JohnDeerToken.objects.create(
            token_type=token['token_type'],
            expires_in=token['expires_in'],
            access_token=token['access_token'],
            refresh_token=token['refresh_token'],
            expires_at=token['expires_at'],
            scope=token['scope']
        )

        return token['access_token']

    except Exception as e:
        logging.exception(e)


def get_valid_token():
    try:
        last_token = JohnDeerToken.objects.order_by('id').last()
        if last_token.expires_at > datetime.timestamp(datetime.now()):
            return last_token.access_token
        else:
            return refresh_access_token(last_token.refresh_token)

    except Exception as e:
        logging.exception(e)
