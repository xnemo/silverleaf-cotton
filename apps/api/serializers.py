from rest_framework import serializers

from main.models import Farm, Organization, Module, Field, Equipment, Mound, ModuleTrace, BaleTrace


class FarmSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")

    class Meta:
        model = Farm
        fields = ('id', 'name', 'district', 'john_deere_id', 'created_at', 'archived')


class FieldSerializer(serializers.ModelSerializer):
    farm = serializers.CharField(source='get_farm', read_only=True)
    modules_count = serializers.IntegerField()
    created_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")
    last_modified_time = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")

    class Meta:
        model = Field
        fields = ('id', 'name', 'farm', 'john_deere_id', 'modules_count', 'last_modified_time', 'created_at')


class ModuleSerializer(serializers.ModelSerializer):
    farm = serializers.CharField(source='get_farm', read_only=True)
    field = serializers.CharField(source='get_field', read_only=True)
    created_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")

    class Meta:
        model = Module
        fields = ('id', 'serial_number', 'uid', 'variety_name', 'weight', 'field', 'farm', 'wrap_date_time',
                  'created_at', 'state')


class EquipmentSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")

    class Meta:
        model = Equipment
        fields = ('id', 'name', 'john_deere_id', 'vin', 'equipment_make', 'equipment_type', 'equipment_apex_type',
                  'equipment_model', 'created_at')


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ('id', 'name', 'type', 'john_deere_id', 'member', 'internal')


class ModuleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ('id', 'uid')


class ModuleInFieldSerializer(serializers.ModelSerializer):
    time = serializers.DateTimeField(source='register_in_field_time')
    lat = serializers.FloatField(source='in_field_lat')
    long = serializers.FloatField(source='in_field_long')

    class Meta:
        model = Module
        fields = ('time', 'lat', 'long')


class ModulePickSerializer(serializers.ModelSerializer):
    time = serializers.DateTimeField(source='picked_from_field_time')
    lat = serializers.FloatField(source='pick_lat')
    long = serializers.FloatField(source='pick_long')

    class Meta:
        model = Module
        fields = ('time', 'lat', 'long')


class MoundListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mound
        fields = ('id', 'uid', 'name')


class ModuleReceptionSerializer(serializers.ModelSerializer):
    time = serializers.DateTimeField(source='load_time')

    class Meta:
        model = Module
        fields = ('time', 'mound')


class ModuleTraceSerializer(serializers.ModelSerializer):

    class Meta:
        model = ModuleTrace
        fields = ('serial_number', 'time', 'lat', 'long', 'action', 'mound_uid')


class MoundProcessStartSerializer(serializers.ModelSerializer):
    state = serializers.HiddenField(default=2)

    class Meta:
        model = Mound
        fields = ('start_production_time', 'state')


class GinReceptionSerializer(serializers.ModelSerializer):
    action = serializers.HiddenField(default='ENTER_TO_GIN')

    class Meta:
        model = ModuleTrace
        fields = ('serial_number', 'time', 'lat', 'long', 'action', 'truck_id', 'position')


class BaleTraceSerializer(serializers.ModelSerializer):
    action = serializers.HiddenField(default='GIN_STORAGE')

    class Meta:
        model = BaleTrace
        fields = ('bale_uid', 'time', 'lat', 'long', 'action')


class ProcessFinishSerializer(serializers.ModelSerializer):
    action = serializers.HiddenField(default='GIN_PROCESS_FINISH')

    class Meta:
        model = BaleTrace
        fields = ('bale_uid', 'time', 'lat', 'long', 'action')
