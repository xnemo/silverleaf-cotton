from django.db import models
from main.queryset.page import PageQuerySet

FarmManager = models.Manager.from_queryset(PageQuerySet)
