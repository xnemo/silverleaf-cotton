from datetime import datetime
import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import BaseModel
from main.managers.bale import BaleManager
from main.managers.farm import FarmManager
from main.managers.field import FieldManager
from main.managers.module import ModuleManager
from main.managers.module_trace import ModuleTraceManager
from main.managers.mound import MoundManager


def year_choices():
    return [r for r in range(datetime.today().year-10, datetime.today().year+1)]


class Country(BaseModel):
    code = models.CharField(_('Country code'), max_length=3, blank=True)
    name = models.CharField(_('Country name'), max_length=255, blank=False)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __str__(self):
        return self.name


class Region(BaseModel):
    code = models.CharField(_('Region code'), max_length=50, blank=False)
    name = models.CharField(max_length=255, blank=False)
    country = models.ForeignKey(Country, related_name='regions', null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _('Region')
        verbose_name_plural = _('Regions')

    def __str__(self):
        return self.name


class District(BaseModel):
    code = models.CharField(_('District code'), max_length=50, blank=False)
    name = models.CharField(_('District name'), max_length=255, blank=False)
    region = models.ForeignKey(Region, related_name='districts', null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _('District')
        verbose_name_plural = _('Districts')

    def __str__(self):
        return self.name


class Organization(BaseModel):
    name = models.CharField(_('Organization name'), max_length=255, blank=False, null=False)
    type = models.CharField(_('Organization type'), max_length=255, blank=True, null=True)
    john_deere_id = models.IntegerField(_('John Deere Id'))
    member = models.BooleanField(_('Member'), default=False)
    internal = models.BooleanField(_('Internal'), default=False)

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')

    def __str__(self):
        return self.name


class Farm(BaseModel):
    name = models.CharField(_('Farm name'), max_length=300, blank=False, null=False)
    description = models.TextField(_('About farm'), blank=True, null=True)
    district = models.ForeignKey(District, related_name='farms', null=True, on_delete=models.SET_NULL)
    john_deere_id = models.CharField(_('John Deere Id'), max_length=255, blank=True, null=True)
    archived = models.BooleanField(default=False)

    objects = FarmManager()

    class Meta:
        verbose_name = _('Farm')
        verbose_name_plural = _('Farms')

    def __str__(self):
        return self.name


class Field(BaseModel):
    name = models.CharField(_('Field name'), max_length=300, blank=False, null=False)
    varieties = models.IntegerField(_('Varieties'), default=1, blank=True, null=True)
    farm = models.ForeignKey(Farm, verbose_name=_('Farm'), related_name='fields', null=True, on_delete=models.SET_NULL)
    geo_json = models.TextField(_('GeoJson'), blank=False)
    john_deere_id = models.CharField(_('John Deere Id'), max_length=255, blank=True, null=True)
    archived = models.BooleanField(default=False)
    last_modified_time = models.DateTimeField(_('Last Modified Time'))
    farm_uid = models.CharField(_('Farm UID'), max_length=255, blank=True, null=True)

    objects = FieldManager()

    class Meta:
        verbose_name = _('Field')
        verbose_name_plural = _('Fields')

    def __str__(self):
        return self.name

    def get_farm(self):
        return self.farm.name


class Equipment(BaseModel):
    name = models.CharField(_('Name'), max_length=255, blank=False, null=False)
    guid = models.CharField(_('GUID'), max_length=100, blank=False, null=False)
    john_deere_id = models.CharField(_('John Deere Id'), max_length=255, blank=True, null=True)
    description = models.TextField(_('Description'), blank=False)
    equipment_make = models.CharField(_('Equipment make name'), max_length=255, blank=True, null=True)
    equipment_make_erid = models.CharField(_('Equipment make ERID'), max_length=255, blank=True, null=True)
    equipment_type = models.CharField(_('Equipment type'), max_length=255, blank=True, null=True)
    equipment_type_guid = models.CharField(_('Equipment type GUID'), max_length=255, blank=True, null=True)
    equipment_type_id = models.CharField(_('Equipment type ID'), max_length=50, blank=True, null=True)
    equipment_apex_type = models.CharField(_('Equipment apex type'), max_length=255, blank=True, null=True)
    equipment_apex_type_guid = models.CharField(_('Equipment apex type GUID'), max_length=255, blank=True, null=True)
    equipment_apex_type_id = models.CharField(_('Equipment apex type ID'), max_length=50, blank=True, null=True)
    equipment_model = models.CharField(_('Equipment model'), max_length=255, blank=True, null=True)
    equipment_model_guid = models.CharField(_('Equipment model GUID'), max_length=255, blank=True, null=True)
    equipment_model_id = models.CharField(_('Equipment model ID'), max_length=255, blank=True, null=True)
    vin = models.CharField(_('Vin'), max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = _('Equipment')
        verbose_name_plural = _('Equipments')

    def __str__(self):
        return self.name


class Mound(BaseModel):

    class State(models.IntegerChoices):
        NEW = 1
        IN_PROCESS = 2
        PROCESSED = 3
        ARCHIVED = 4

    uid = models.CharField(_('UID'), max_length=100, blank=False, null=False)
    name = models.CharField(_('Name'), max_length=255, blank=False, null=False)
    mound_weight = models.FloatField(_('Mound weight'), default=0, null=True)
    farm = models.ForeignKey(Farm, verbose_name=_('Farm'), related_name='mounds', null=True, on_delete=models.SET_NULL)
    field = models.ForeignKey(Field, verbose_name=_('Field'), related_name='mounds', null=True,
                              on_delete=models.SET_NULL)
    start_production_time = models.DateTimeField(_('Start production timestamp'), null=True)
    end_production_time = models.DateTimeField(_('End production timestamp'), null=True)
    archived_time = models.DateTimeField(_('Removed timestamp'), null=True)
    state = models.IntegerField(choices=State.choices, default=State.NEW)

    objects = MoundManager()

    class Meta:
        verbose_name = _('Mound')
        verbose_name_plural = _('Mounds')

    def __str__(self):
        return self.name


class Module(BaseModel):

    class State(models.IntegerChoices):
        NEW = 1
        IN_PROCESS = 2
        PROCESSED = 3

    uid = models.CharField(_('ModuleID'), max_length=100, blank=False, null=False)
    serial_number = models.CharField(_('Serial number'), max_length=100, blank=True, null=True)
    wrap_lat = models.FloatField(_('Wrap lat'), null=True)
    wrap_long = models.FloatField(_('Wrap long'), null=True)
    wrap_date_time = models.DateTimeField(_('Wrap datetime'))
    data_ingestion_date = models.DateTimeField(_('Data ingestion date'))
    tag_count = models.IntegerField(_('Tag count'))
    variety_name = models.CharField(_('Variety name'), max_length=255, blank=True, null=True)
    machine_pin = models.CharField(_('Machine pin'), max_length=255, blank=True, null=True)
    operator = models.CharField(_('Operator'), max_length=255, blank=True, null=True)
    moisture = models.FloatField(_('Moisture'), null=True)
    moisture_unit = models.CharField('Moisture unit', max_length=100, blank=True, null=True)
    diameter = models.FloatField(_('Diameter'), blank=True, null=True)
    diameter_unit = models.CharField('Diameter unit', max_length=100, blank=True, null=True)
    weight = models.FloatField(_('Weight'), blank=True, null=True)
    weight_unit = models.CharField('Weight unit', max_length=100, blank=True, null=True)
    drop_lat = models.FloatField(_('Drop lat'), null=True)
    drop_long = models.FloatField(_('Drop long'), null=True)
    incremental_area = models.FloatField(_('Incremental area'), null=True)
    area_unit = models.CharField('Area unit', max_length=100, blank=True, null=True)
    field_uid = models.CharField(_('Field UID'), max_length=100, blank=True, null=True)
    farm = models.ForeignKey(Farm, verbose_name=_('Farm'), related_name='modules', null=True, on_delete=models.SET_NULL)
    field = models.ForeignKey(Field, verbose_name=_('Field'), related_name='modules', null=True,
                              on_delete=models.SET_NULL)
    equipment = models.ForeignKey(Equipment, verbose_name=_('Equipment'), related_name='modules', null=True,
                                  on_delete=models.SET_NULL)
    mound = models.ForeignKey(Mound, verbose_name=_('Mound'), related_name='modules', null=True, on_delete=models.SET_NULL)
    load_time = models.DateTimeField(_('Loaded to mound time'), null=True)
    start_production = models.DateTimeField(_('Start production timestamp'), null=True)
    end_production = models.DateTimeField(_('End production timestamp'), null=True)
    state = models.IntegerField(choices=State.choices, default=State.NEW)
    register_in_field_time = models.DateTimeField(_('Register on field'), null=True)
    in_field_lat = models.FloatField(_('In field lat'), null=True)
    in_field_long = models.FloatField(_('In field long'), null=True)
    picked_from_field_time = models.DateTimeField(_('Picked from field'), null=True)
    pick_lat = models.FloatField(_('Pick lat'), null=True)
    pick_long = models.FloatField(_('Pick long'), null=True)

    objects = ModuleManager()

    class Meta:
        verbose_name = _('Module')
        verbose_name_plural = _('Modules')

    def __str__(self):
        return f'{self.uid}'

    def get_farm(self):
        return self.farm.name

    def get_field(self):
        return self.field.name


class MoundModules(BaseModel):
    scan_date = models.DateTimeField(_('Scan date'))
    farm = models.ForeignKey(Farm, verbose_name=_('Farm'), related_name='moundmodules', null=True,
                             on_delete=models.SET_NULL)
    field = models.ForeignKey(Field, verbose_name=_('Field'), related_name='moundmodules', null=True,
                              on_delete=models.SET_NULL)
    module = models.ForeignKey(Module, verbose_name=_('Module'), related_name='moundmodules', null=True,
                               on_delete=models.SET_NULL)
    mound = models.ForeignKey(Mound, verbose_name=_('Mounds'), related_name='moundmodules', null=True,
                              on_delete=models.SET_NULL)
    module_weight = models.FloatField(_('Module weight'), default=0, null=True)
    uid = models.CharField(_('UID'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('MoundModules')
        verbose_name_plural = _('MoundModules')

    def __str__(self):
        return str(self.scan_date)


class ModuleTrace(BaseModel):
    class State(models.IntegerChoices):
        NEW = 1
        IN_PROCESS = 2
        PROCESSED = 3

    serial_number = models.TextField(_('Serial number'), max_length=100, blank=True, null=True)
    time = models.DateTimeField(_('Time'), null=True)
    lat = models.FloatField(_('Lat'), null=True)
    long = models.FloatField(_('Long'), null=True)
    action = models.TextField(_('Action'), max_length=100, blank=True, null=True)
    mound_uid = models.TextField(_('Mound UID'), max_length=100, blank=True, null=True)
    truck_id = models.TextField(_('Truck ID'), max_length=100, blank=True, null=True)
    position = models.IntegerField(_('Position'), default=0)
    moisture = models.FloatField(_('Moisture'), default=0, null=True)
    state = models.IntegerField(choices=State.choices, default=State.NEW)

    objects = ModuleTraceManager()

    class Meta:
        verbose_name = _('Module trace')
        verbose_name_plural = _('Module traces')

    def __str__(self):
        return self.serial_number


class Bale(BaseModel):
    print_date = models.DateTimeField(_('Print date'), default=datetime.now)
    production_date = models.DateTimeField(_('Production date'), default=datetime.now)
    module = models.ForeignKey(Module, verbose_name=_('Module'), related_name='bales', null=True,
                               on_delete=models.SET_NULL)
    mound = models.ForeignKey(Mound, verbose_name=_('Mounds'), related_name='bales', null=True,
                              on_delete=models.SET_NULL)
    uid = models.TextField(_('Unique Bale ID'), blank=True)
    weight = models.FloatField(_('Weight'), blank=True, null=True)
    module_trace = models.ForeignKey(ModuleTrace, verbose_name=_("Module trace"), related_name='bales', null=True,
                                     on_delete=models.SET_NULL)

    objects = BaleManager()

    class Meta:
        verbose_name = _('Bale')
        verbose_name_plural = _('Bale')

    def __str__(self):
        return self.uid


class BaleTrace(BaseModel):
    module = models.CharField(_('Module serial number'), max_length=100, blank=True, null=True)
    bale_uid = models.TextField(_('Unique Bale ID'), blank=True)
    time = models.DateTimeField(_('Time'))
    lat = models.FloatField(_('Lat'), null=True)
    long = models.FloatField(_('Long'), null=True)
    description = models.TextField(_('Description'), blank=True, null=True)
    action = models.CharField(_('Action'), max_length=255, blank=True, null=True)
    state = models.CharField(_('State'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('Bale trace')
        verbose_name_plural = _('Bale traces')

    def __str__(self):
        return self.bale_uid


