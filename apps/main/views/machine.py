from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from main.models import Equipment


@login_required
def list(request):
    machines = Equipment.objects.all().order_by('id')
    return render(request, "main/machines.html", {'data': machines})