from django.db import models

from main.queryset.mound import MoundQuerySet

MoundManager = models.Manager.from_queryset(MoundQuerySet)
