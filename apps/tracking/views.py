from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render

from main.models import Bale


def index(request):
    try:
        bale = Bale.objects.get(uid=request.GET.get('tracking', ''))
        return render(request, "tracking/index.html", {'data': bale})
    except ObjectDoesNotExist:
        return render(request, "tracking/index.html")

