from django.contrib.postgres.search import SearchVector
from rest_framework import filters
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from api.pagination import DatatablePagination
from api.queryset.datatable import filter_for_datatable
from api.serializers import ModuleListSerializer, ModuleSerializer
from main.models import Module


class ModuleView(ListAPIView):
    serializer_class = ModuleListSerializer
    pagination_class = PageNumberPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('id', 'uid')
    ordering = ['pk']

    def get_queryset(self):
        return Module.objects.all()


class ModuleDatatableView(ListAPIView):
    serializer_class = ModuleSerializer
    pagination_class = DatatablePagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)

    def get_queryset(self):
        return Module.objects.all()

    def list(self, request, *args, **kwargs):
        draw = request.query_params.get('draw')
        queryset = self.filter_queryset(self.get_queryset())
        records_total = queryset.count()
        search_vector = SearchVector('serial_number', 'uid', 'field__name', 'farm__name', 'variety_name',
                                     'machine_pin', 'weight', 'diameter', 'created_at')
        filtered_queryset = filter_for_datatable(request, queryset, search_vector)
        start = int(request.query_params.get('start', 0))
        length = int(request.query_params.get('length', 10))
        end = length + start
        serializer = self.get_serializer(filtered_queryset[start:end], many=True)
        response = {
            'draw': draw,
            'recordsTotal': records_total,
            'recordsFiltered': filtered_queryset.count(),
            'data': serializer.data
        }
        return Response(response)
