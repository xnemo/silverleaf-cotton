from rest_framework.generics import UpdateAPIView, get_object_or_404, CreateAPIView
from rest_framework.response import Response
from rest_framework import status

from api.serializers import MoundProcessStartSerializer, GinReceptionSerializer, BaleTraceSerializer, \
    ProcessFinishSerializer
from main.models import Mound


class ProcessStartView(UpdateAPIView):
    http_method_names = ['put']
    serializer_class = MoundProcessStartSerializer

    def put(self, request, uid):
        mound = get_object_or_404(Mound, uid=uid)
        serializer = self.serializer_class(mound, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)


class GinningReceptionView(CreateAPIView):
    serializer_class = GinReceptionSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class GinningStorageView(CreateAPIView):
    serializer_class = BaleTraceSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class GinningProcessFinishView(CreateAPIView):
    serializer_class = ProcessFinishSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)
