from django.db.models import Count

from main.queryset.page import PageQuerySet


class FieldQuerySet(PageQuerySet):

    def get_archived(self, archived):
        return super().filter(archived=archived)

    def get_has_modules(self):
        return super().annotate(modules_count=Count('modules')).filter(modules_count__gt=0)