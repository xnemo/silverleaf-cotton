from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from main.models import Organization


@login_required
def list(request):
    organizations = Organization.objects.all().order_by('id')
    return render(request, "main/organizations.html", {'data': organizations})