import time

from core.utils.requests import requests_retry_session
from johndeere.services.auth import get_valid_token
from main.models import Equipment


def get_machines(organization_id):
    token = get_valid_token()
    headers = {'Authorization': 'Bearer ' + token, 'Accept': 'application/vnd.deere.axiom.v3+json'}
    has_next_page = True
    url = f"https://sandboxapi.deere.com/platform/organizations/{organization_id}/machines;start=0;count=100"
    while has_next_page:
        result = requests_retry_session().get(url, headers=headers).json()
        has_next_page = False
        for item in result['values']:
            values_for_update = {
                'name': item['name'],
                'john_deere_id': item['id'],
                'guid': item['GUID'],
                'equipment_make': item['equipmentMake']['name'],
                'equipment_make_erid': item['equipmentMake']['ERID'],
                'equipment_type': item['equipmentType']['name'],
                'equipment_type_guid': item['equipmentType']['GUID'],
                'equipment_type_id': item['equipmentType']['id'],
                'equipment_apex_type': item['equipmentApexType']['name'],
                'equipment_apex_type_guid': item['equipmentApexType']['GUID'],
                'equipment_apex_type_id': item['equipmentApexType']['id'],
                'equipment_model': item['equipmentModel']['name'],
                'equipment_model_guid': item['equipmentModel']['GUID'],
                'equipment_model_id': item['equipmentModel']['id'],
                'vin': item['vin']
            }
            Equipment.objects.update_or_create(john_deere_id=item['id'], defaults=values_for_update)

        for link in result['links']:
            if link['rel'] == 'nextPage':
                url = link['uri']
                has_next_page = True

        time.sleep(2)
