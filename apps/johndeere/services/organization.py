import requests

from johndeere.services.auth import get_valid_token
from main.models import Organization


def get_organizations() -> str:
    url = 'https://sandboxapi.deere.com/platform/organizations/'
    token = get_valid_token()
    headers = {'Authorization': 'Bearer ' + token, 'Accept': 'application/vnd.deere.axiom.v3+json'}
    result = requests.get(url, headers=headers).json()

    for item in result['values']:
        values_for_update = {
            'name': item['name'],
            'john_deere_id': item['id'],
            'type': item['type'],
            'member': item['member'],
            'internal': item['internal']
        }
        Organization.objects.update_or_create(john_deere_id=item['id'], defaults=values_for_update)
    return result
