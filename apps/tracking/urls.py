from django.urls import path
from tracking.views import index

urlpatterns = [
    path('', index, name='home'),
]