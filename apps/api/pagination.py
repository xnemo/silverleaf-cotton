import math

from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination
from rest_framework.response import Response


class ResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'limit'
    max_page_size = 1000
    filters = None

    def get_paginated_response(self, data):
        page_size = int(self.request.query_params.get('limit', self.page_size))
        return Response({
            'pagination': {
                'currentPage': self.page.number,
                'lastPage': self.page.paginator.num_pages,
                'perPage': page_size,
                'total': self.page.paginator.count,
            },
            'results': data,
            'filters': self.filters
        })

    @classmethod
    def set_filter(cls, filters):
        cls.filters = filters


class DatatablePagination(LimitOffsetPagination):
    default_limit = 10
    limit_query_param = 'length'
    offset_query_param = 'start'
    max_limit = None

    def get_paginated_response(self, data):
        page_size = int(self.request.query_params.get('length', self.default_limit))
        return Response({
            'draw': self.request.query_params.get('draw', 1),
            'recordsTotal': self.count,
            'recordsFiltered': self.count,
            'data': data,
            'pageSize': page_size
        })


class LargeResultsSetPagination(ResultsSetPagination):
    page_size = 1000