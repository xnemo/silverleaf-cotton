from django.contrib import admin

from johndeere.models import JohnDeerToken


@admin.register(JohnDeerToken)
class JohnDeerTokenAdmin(admin.ModelAdmin):
    list_display = ('token_type', 'expires_in', 'scope', 'created_at', 'updated_at', 'expires_at')
    fields = ('token_type', 'expires_in', 'access_token', 'scope', 'refresh_token')
    search_fields = ('token_type', 'expires_in', 'scope', 'created_at', 'updated_at')
