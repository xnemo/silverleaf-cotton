from django.forms import ModelForm

from main.models import Mound, Bale, Farm, ModuleTrace


class MoundForm(ModelForm):

    class Meta:
        model = Mound
        fields = ['field', 'name', 'uid']


class ProductionForm(ModelForm):

    class Meta:
        model = Bale
        fields = ['mound', 'module_trace', 'weight']


class FarmForm(ModelForm):

    class Meta:
        model = Farm
        fields = ['description', 'district']

