from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse

from johndeere.services.auth import authorize, get_token
from johndeere.services.machines import get_machines
from johndeere.services.organization import get_organizations
from johndeere.tasks import get_farms_task, get_fields_task, get_modules_task


@login_required
def auth(request):
    authorization_url, state = authorize()
    request.session['oauth_state'] = state
    return redirect(authorization_url)


def callback(request):
    get_token(state=request.session['oauth_state'], uri=request.build_absolute_uri())
    return HttpResponseRedirect("/")


@login_required
def organization_sync(request):
    get_organizations()
    return redirect(reverse('organization-list'))


@login_required
def farms_sync(request):
    get_farms_task.delay(422753)
    return redirect(reverse('farms-list'))


@login_required
def fields_sync(request):
    get_fields_task.delay(422753)
    return redirect(reverse('fields-list'))


@login_required
def machines_sync(request):
    get_machines(422753)
    return redirect(reverse('machines-list'))


@login_required
def modules_sync(request):
    get_modules_task.delay(422753)
    return redirect(reverse('modules-list'))
