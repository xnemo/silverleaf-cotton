def filter_for_datatable(request, queryset, search_vector):
    search_query = request.query_params.get('search[value]')
    if search_query:
        queryset = queryset.annotate(search=search_vector).filter(search__icontains=search_query)
    ordering_column = request.query_params.get('order[0][column_name]', 'id')
    ordering_direction = request.query_params.get('order[0][dir]')
    if ordering_direction == 'desc':
        ordering_column = f"-{ordering_column}"
    queryset = queryset.order_by(ordering_column)
    return queryset
