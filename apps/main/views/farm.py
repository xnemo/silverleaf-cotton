from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render

from main.models import Farm


@login_required
def list(request):
    data = Farm.objects.all().order_by('id').per_page(request, 10)
    return render(request, "main/farm/list.html", {'data': data})


@login_required
def json_list(request):
    data = Farm.objects.all().order_by('id').values()
    # serializer = FarmSerializer(data)
    # data = serializers.serialize('json', data)
    return JsonResponse({
        'recordsTotal': 0,
        'recordsFiltered': 0,
        'data': data})
