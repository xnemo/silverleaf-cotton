import time

from core.utils.requests import requests_retry_session
from johndeere.services.auth import get_valid_token
from main.models import Field, Farm


def get_fields(organization_id, logger):
    token = get_valid_token()
    headers = {'Authorization': 'Bearer ' + token, 'Accept': 'application/vnd.deere.axiom.v3+json'}
    has_next_page = True
    url = f"https://sandboxapi.deere.com/platform/organizations/{organization_id}/fields;start=0;count=100?embed=farms&recordFilter=ALL"
    while has_next_page:
        result = requests_retry_session().get(url, headers=headers).json()
        has_next_page = False
        for item in result['values']:
            farm_id, farm_uid = None, item['farms']['farms'][0]['id']
            logger.info(farm_uid)
            farm = Farm.objects.filter(john_deere_id=farm_uid).first()
            if farm:
                farm_id = farm.id

            values_for_update = {
                'name': item['name'],
                'john_deere_id': item['id'],
                'archived': item['archived'],
                'last_modified_time': item['lastModifiedTime'],
                'farm_id': farm_id,
                'farm_uid': farm_uid
            }
            Field.objects.update_or_create(john_deere_id=item['id'], defaults=values_for_update)

        for link in result['links']:
            if link['rel'] == 'nextPage':
                url = link['uri']
                has_next_page = True

        time.sleep(2)
