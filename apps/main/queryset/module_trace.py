from main.queryset.page import PageQuerySet


class ModuleTraceQuerySet(PageQuerySet):

    def get_reception_modules(self):
        return super().filter(action='ENTER_TO_GIN').order_by('id', 'position')
