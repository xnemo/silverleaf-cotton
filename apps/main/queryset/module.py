from main.queryset.page import PageQuerySet


class ModuleQuerySet(PageQuerySet):

    def get_unloaded(self, field_id):
        return super().filter(field__id=field_id, mound__isnull=True)